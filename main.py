from pynput.keyboard import Key, Listener
from classes.Game import Game

game = Game()

def on_release(key):
    if key == Key.esc:
        # Stop listener
        return False
    game.update(key)
    if (key == Key.left) | (key == Key.up) | (key == Key.down) | (key == Key.right):
        game.turn += 1

# Collect events until released
with Listener(
        on_release=on_release) as listener:
    listener.join()
