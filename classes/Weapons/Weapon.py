from classes.Items.Item import Item

# weapon_type = O = contondant, / = slash, | = pierce

class Weapon(Item):
    def __init__(self, name):
        super(Weapon, self).__init__(name)

        self.DMG = 1
        self.type = 'O'
