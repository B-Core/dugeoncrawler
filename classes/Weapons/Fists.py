from classes.Weapons.Weapon import Weapon

class Fists(Weapon):
    def __init__(self, name):
        super(Fists, self).__init__(name + ' Fists')
