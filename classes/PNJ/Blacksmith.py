from classes.Entities.Person import Person

class Blacksmith(Person):
    def __init__(self, name, position):
        super(Blacksmith, self).__init__(name, position)
        self.symbol = 'B';
