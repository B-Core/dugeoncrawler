import json
import os
import os.path
from sys import argv
from terminaltables import AsciiTable

basePath = os.path.realpath(argv[0].replace('/main.py', ''))

config = {
    "SYMBOLS_PATH" : os.path.join(basePath, "ressources/symbols.json"),
}

class Renderer(object):
    def __init__(self):
        self.table_instance = AsciiTable([[]])

        jsonFile = open(config["SYMBOLS_PATH"])
        self.__SYMBOLS = json.loads(jsonFile.read())

        jsonFile.close()

        self.table_instance = AsciiTable([[]])
        self.table_instance.justify_columns = {0: 'center', 1: 'center', 2: 'center'}
        self.table_instance.inner_heading_row_border = False
        self.table_instance.heading_row_border = False
        self.table_instance.column_border = False
        self.table_instance.inner_column_border = False
        self.table = self.table_instance.table

    def replaceSymbol(self, caracter):
        return self.__SYMBOLS[caracter] if caracter in self.__SYMBOLS else caracter

    def combineTables(self, level):
        for index, line in enumerate(level):
            for c_index, caracter in enumerate(line):
                level[index][c_index] = self.replaceSymbol(caracter)

        self.table_instance.table_data = level
        self.table = self.table_instance.table

    def positionEntitie(self, level, entitie):
        level[entitie.position["y"]][entitie.position["x"]] = entitie.symbol

    def update(self, level, entities):
        os.system('cls' if os.name == 'nt' else 'clear')
        for entitie in entities:
            self.positionEntitie(level, entitie)

        self.combineTables(level)
        # print current_level
        print self.table
