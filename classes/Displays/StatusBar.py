from terminaltables import AsciiTable

class StatusBar:
    def __init__(self):
        self.table_data = [
            ['Level Name', 'Health', 'Mana', 'Gold Piece'],
            ['***', '0', '0', '0'],
        ]
        self.title = "Status Bar"
        self.update(self.table_data[1])

    def update(self, data):
        self.table_data[1] = data
        self.table_instance = AsciiTable(self.table_data, self.title)
        self.table = self.table_instance.table
        print self.table
