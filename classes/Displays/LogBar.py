from terminaltables import AsciiTable

class LogBar(object):
    def __init__(self):
        self.logs = []
        self.active_logs = [[]]
        self.turn = 0
        self.offset = 0
        self.table_instance = AsciiTable(self.logs, 'Log-Bar')
        self.update('', 0)

    def update(self, key, turn):
        self.turn = turn
        if key == "u'<'":
            self.previousLogs(10)
        elif key == "u'>'":
            self.nextLogs(10)
        else:
            self.active_logs = self.logs[0:10]

        if len(self.active_logs) > 0:
            self.active_logs[0][0] = self.turn
        self.table_instance.table_data = self.active_logs
        print self.table_instance.table

    def addLog(self, log):
        self.logs.insert(0, [' ', log])
        if len(self.logs) > 1000:
            self.logs = self.logs[0:len(self.logs)/2]

    def previousLogs(self, offset):
        if self.offset + offset <= len(self.logs):
            self.offset += offset
        else:
            self.offset = len(self.logs)
        self.active_logs = self.logs[self.offset - offset:self.offset]

    def nextLogs(self, offset):
        if self.offset - offset >= 0:
            self.offset -= offset
        else:
            self.offset = 0
        self.active_logs = self.logs[self.offset:self.offset+10]
