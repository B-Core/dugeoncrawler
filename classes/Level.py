import json
import os.path
from sys import argv

basePath = os.path.realpath(argv[0].replace('/main.py', ''))

config = {
    "MAP_TEMPLATES_PATH" : os.path.join(basePath, "ressources/maps-templates.json"),
}

class Level(object):
    def __init__(self, name):
        self.name = name
        self.refresh()
        self.max_map_x = len(self.map[0]) - 1
        self.max_map_y = len(self.map) - 1

    def refresh(self):
        jsonFile = open(config["MAP_TEMPLATES_PATH"])
        self.map_templates = json.loads(jsonFile.read())[self.name]
        self.config = self.map_templates['config']
        self.map = self.map_templates['map']
        jsonFile.close()
