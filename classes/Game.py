import random
from classes.Level import Level
from classes.Entities.Player import Player
from classes.Displays.Renderer import Renderer
from classes.Displays.StatusBar import StatusBar
from classes.Displays.LogBar import LogBar

from classes.EntitieConstructor import EntitieConstructor

def clamp(n, minn, maxn):
    return max(min(maxn, n), minn)

class Game(object):
    def __new__(cls, *args, **kargs):
        print "Creating instance of Game"
        return super(Game, cls).__new__(cls)

    def __init__(self):
        self.turn = 0
        self.entities = []
        self.loadLevel("TOWN")
        self.renderer = Renderer()
        self.status_bar = StatusBar()
        self.log_bar = LogBar()
        self.__EntitieConstructor = EntitieConstructor()

        name = raw_input("What's your Name For this adventure ? : ")
        self.player = Player(name, {
            "x": 10,
            "y": 5,
        })
        self.addEntities(self.player)
        self.loadEntities()
        self.generateMonsters(self.currentLevel.config)

        self.update()

    def update(self, *arg):
        strKey = '{0}'.format(arg[0] if len(arg) == 1 else "")

        #Controls
        self.player.control(strKey)
        self.log_bar.addLog(''.join([self.player.name, ' moved ', strKey]))

        # rendering update
        self.currentLevel.refresh()

        # Game update
        self.checkCollisions(strKey)

        self.renderer.update(self.currentLevel.map, self.entities)

        self.status_bar.update(self.getPlayerStatus())
        self.log_bar.update(strKey, self.turn)

        print strKey

    def loadLevel(self, levelName):
        self.currentLevel = Level(levelName)

    def loadEntities(self):
        entitie_list = self.currentLevel.map_templates['entities']
        for index,entitie in enumerate(entitie_list.keys()):
            entitieName = entitie_list[entitie]['name'] if any("name" in s for s in entitie_list) else entitie
            newEntitie = self.__EntitieConstructor.create(entitie, { 'name': entitieName, 'position': entitie_list[entitie]['position'] })
            if(newEntitie):
                self.addEntities(newEntitie)

    def generateMonsters(self, config):
        for numberOfMonster in range(0, config['numberOfMonsters']):
            type_of_monsters = config['typeOfMonsters']
            className = type_of_monsters[random.randint(0, len(config['typeOfMonsters']) - 1)]
            newEntitie = self.__EntitieConstructor.create(className, {
                "name": className,
                "position": {
                    "x": random.randint(0, self.currentLevel.max_map_x),
                    "y": random.randint(0, self.currentLevel.max_map_y),
                }
            })
            self.addEntities(newEntitie)

    def addEntities(self, entitie):
        if type(entitie) != None:
            self.entities.append(entitie)
    def removeEntitie(self, entitie):
        self.entities.remove(entitie)

    def getPlayerStatus(self):
        return [self.currentLevel.name, str(self.player.health) + ' / ' + str(self.player.max_health), str(self.player.mana) + ' / ' + str(self.player.max_mana), self.player.gp]

    def checkCollisions(self, key):
        inverted_key = ''
        if key == 'Key.up':
            inverted_key = 'Key.down'
        elif key == 'Key.down':
            inverted_key = 'Key.up'
        elif key == 'Key.left':
            inverted_key = 'Key.right'
        elif key == 'Key.right':
            inverted_key = 'Key.left'

        for entitie in self.entities:
            if entitie.name != self.player.name:
                self.currentLevel.refresh()
                if entitie.position['x'] == self.player.position['x'] and entitie.position['y'] == self.player.position['y']:
                    self.log_bar.addLog(''.join([self.player.name, ' collided with ', entitie.name]))
                    self.player.control(inverted_key)
                else:
                    entitie.update()

            entitie.position['x'] = clamp(entitie.position['x'], 0, self.currentLevel.max_map_x)
            entitie.position['y'] = clamp(entitie.position['y'], 0, self.currentLevel.max_map_y)
            self.renderer.update(self.currentLevel.map, self.entities)
