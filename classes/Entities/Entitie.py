from classes.Weapons.Fists import Fists

class Entitie(object):
    def __init__(self, *symbol):
        self.symbol = symbol[0] if len(symbol) > 0 else '@';
        self.name = "Entitie"

        self.max_health = 1
        self.health = 1

        self.attributes = {
            "strength": 1,
            "dexterity": 1,
            "constitution": 1,
            "intelligence": 0,
            "charisma": 0,
        }

        self.DMG = 0
        self.mana = 0
        self.max_mana = 0

        self.currentWeapon = Fists(self.name)

        self.dead = False
        self.inventory = []

    def update(self):
        self.calculateStatitics()

    def calculateStatitics(self):
        self.dead = self.health == 0

        self.DMG = self.strength * self.currentWeapon.DMG
