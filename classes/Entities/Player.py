from classes.Entities.Person import Person

class Player(Person):
    def __init__(self, name, position):
        super(Player, self).__init__(name, position)
        self.gp = 0

    def control(self, key):
        if key == 'Key.up':
            self.position['y'] -= 1
        elif key == 'Key.down':
            self.position['y'] += 1
        elif key == 'Key.left':
            self.position['x'] -= 1
        elif key == 'Key.right':
            self.position['x'] += 1
