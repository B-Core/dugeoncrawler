from classes.Entities.Entitie import Entitie

class Person(Entitie):
    def __init__(self, name, position):
        super(Person, self).__init__()
        self.name = name
        self.position = position

    def update(self, *position):
        self.position = position[0] if len(position) > 0 else self.position

    def collide(self, collidingEntitie):
        print collidingEntitie
