import random
from classes.Entities.Entitie import Entitie

class Monster(Entitie):
    def __init__(self, name, position):
        super(Monster, self).__init__()
        self.name = name

        self.position = position
        self.symbol = 'M'

    def update(self, *position):
        self.walk()

    def walk(self):
        xOrY = random.randint(0,1)

        if xOrY > 0:
            self.position['x'] += random.randint(-1,1)
        else:
            self.position['y'] += random.randint(-1,1)
