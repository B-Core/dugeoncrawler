from classes.Entities.Entitie import Entitie
from classes.Entities.Person import Person
from classes.Monsters.Monster import Monster
from classes.Monsters.Bat import Bat
from classes.Monsters.Rat import Rat
from classes.PNJ.Blacksmith import Blacksmith

class EntitieConstructor(object):
    def __init__(self):
        self.classesPossible = {
            'Entitie': Entitie,
            'Person': Person,
            'Monster': Monster,
            'Bat': Bat,
            'Rat': Rat,
            'Blacksmith': Blacksmith,
        }

    def create(self, className, params):
        if any(className in s for s in self.classesPossible.keys()):
            return self.classesPossible[className](params['name'], params['position'])
        else:
            print 'no class named %s' % className
